public class TestPalindrome {
String number;
String reverse = "";

    public void isPalindrome(String number){
        this.number = number;
        int length = number.length();
        for(int i=length-1; i>=0;i--){
            reverse = reverse + number.charAt(i);
        }
        if(number.equals(reverse)){
            System.out.println("Number is a Palindrome");
        }
        else
            System.out.println("Number is not a palindrome");
    }
}
